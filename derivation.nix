{ lib, python3Packages }:
with python3Packages;
buildPythonApplication {
  pname = "crypto_trading_bot";
  version = "1.0";

  propagatedBuildInputs = [ pandas requests tkinter ];

  src = ./.;
}
