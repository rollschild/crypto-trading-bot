from setuptools import setup, find_packages

setup(
    name="crypto_trading_bot",
    version="1.0",
    description="A simple crypto trading bot, with GUI",
    packages=find_packages(),
    # Executables
    scripts=["./crypto_trading_bot/main.py"],
)
