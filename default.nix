{ pkgs ? import <nixpkgs> { } }:
pkgs.callPackage ./derivation.nix { }
# let
#   # change this
#   pythonPackageName = "crypto_trading_bot";
#
#   my-python = pkgs.python3;
#   python-with-my-packages =
#     my-python.withPackages (p: with p; [ pandas requests tkinter ]);
# in pkgs.mkShell {
#   buildInputs = [ python-with-my-packages ];
#   shellHook = ''
#     PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}:$PWD:$PWD/${pythonPackageName}
#   '';
# }
