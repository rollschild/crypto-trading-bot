from enum import Enum

BINANCE_API = "https://api.binance.com"
BINANCE_API_EXCHANGE_INFO = "/api/v3/exchangeInfo"
BINANCE_API_ORDER_BOOK_TICKER = "/api/v3/ticker/bookTicker"
BINANCE_API_CANDLESTICK = "/api/v3/klines"
BITMEX_API = "https://www.bitmex.com/api/v1/instrument/active"
BINANCE_TESTNET_API = "https://testnet.binancefuture.com"
BINANCE_TESTNET_EXCHANGE_INFO = "/fapi/v1/exchangeInfo"
BINANCE_TESTNET_ORDER_BOOK_TICKER = "/fapi/v1/ticker/bookTicker"
BINANCE_TESTNET_CANDLESTICK = "/fapi/v1/klines"
BINANCE_WEBSOCKET_API = "wss://fstream.binance.com"
BINANCE_TESTNET_ACCOUNT_INFO_V2 = "/fapi/v2/account"
BINANCE_TESTNET_ORDER = "/fapi/v1/order"


# could also use the StrEnum in python 3.11
class Side(str, Enum):
    BUY = "BUY"
    SELL = "SELL"


class OrderType(str, Enum):
    LIMIT = "LIMIT"
    MARKET = "MARKET"
    STOP = "STOP"
    STOP_MARKET = "STOP_MARKET"
    TAKE_PROFIT = "TAKE_PROFIT"
    TAKE_PROFIT_MARKET = "TAKE_PROFIT_MARKET"
    TRAILING_STOP_MARKET = "TRAILING_STOP_MARKET"


class PositionSide(str, Enum):
    BOTH = "BOTH"
    LONG = "LONG"
    SHORT = "SHORT"


class TimeInForce(str, Enum):
    GTC = "GTC"
    IOC = "IOC"
    FOK = "FOK"
    GTX = "GTX"
