import hmac
import hashlib
import logging
from typing import Optional
import requests
import time
from urllib.parse import urlencode
from crypto_trading_bot.constants import (
    BINANCE_API,
    BINANCE_TESTNET_ACCOUNT_INFO_V2,
    BINANCE_TESTNET_API,
    BINANCE_TESTNET_EXCHANGE_INFO,
    BINANCE_TESTNET_ORDER_BOOK_TICKER,
    BINANCE_TESTNET_CANDLESTICK,
    BINANCE_TESTNET_ORDER,
    OrderType,
    Side,
    TimeInForce,
)

logger = logging.getLogger()


class BinanceFuturesClient:
    def __init__(self, testnet: bool, public_key: str, secret_key: str) -> None:
        if testnet:
            self.base_url = BINANCE_TESTNET_API
        else:
            self.base_url = BINANCE_API

        logger.info("Binance Futures Client successfully initialized!")

        self.public_key = public_key
        self.secret_key = secret_key
        self.headers = {"X-MBX-APIKEY": self.public_key}

        self.prices = {}

    def generate_signature(self, data):
        """Generate signatures for API usage"""

        return hmac.new(
            self.secret_key.encode("utf-8"), urlencode(data).encode(), hashlib.sha256
        ).hexdigest()

    def make_request(self, method: str, endpoint: str, data):
        """Make web requests based on the endpoint and method"""

        if method == "GET":
            response = requests.get(
                self.base_url + endpoint, params=data, headers=self.headers
            )
        elif method == "POST":
            response = requests.post(
                self.base_url + endpoint, params=data, headers=self.headers
            )
        elif method == "DELETE":
            response = requests.delete(
                self.base_url + endpoint, params=data, headers=self.headers
            )
        else:
            raise ValueError()

        if (
            response.status_code == 200
            and response.headers["content-type"] == "application/json"
        ):
            return response.json()

        logger.error(
            "Error making %s request to %s: %s (error code %s)",
            method,
            endpoint,
            response.json(),
            response.status_code,
        )

    def get_contracts(self):
        exchange_info = self.make_request("GET", BINANCE_TESTNET_EXCHANGE_INFO, None)

        contracts = {}

        if exchange_info is not None:
            for contract_data in exchange_info["symbols"]:
                contracts[contract_data["symbol"]] = contract_data

        return contracts

    def get_bid_ask(self, symbol: str):
        data = {}
        data["symbol"] = symbol

        order_book_data = self.make_request(
            "GET", BINANCE_TESTNET_ORDER_BOOK_TICKER, data
        )

        if order_book_data is not None:
            if symbol not in self.prices:
                self.prices[symbol] = {
                    "bid_price": float(order_book_data["bidPrice"]),
                    "ask_price": float(order_book_data["askPrice"]),
                }
            else:
                self.prices[symbol]["bid_price"] = float(order_book_data["bidPrice"])
                self.prices[symbol]["ask_price"] = float(order_book_data["askPrice"])

        return self.prices[symbol]

    def get_historical_candles(self, symbol: str, interval: str):
        data = {}
        data["symbol"] = symbol
        data["interval"] = interval
        data["limit"] = 1000

        raw_candles = self.make_request("GET", BINANCE_TESTNET_CANDLESTICK, data)

        candles = []

        if raw_candles is not None:
            for c in raw_candles:
                # [open_time, open, high, low, close, volume]
                # maybe can be rewritten using list comprehension
                candles.append(list(map(float, c[0:6])))

        return candles

    def get_balances(self):
        data = {}
        data["timestamp"] = int(time.time() * 1000)
        data["signature"] = self.generate_signature(data)

        balances = {}

        account_data = self.make_request(
            "GET", BINANCE_TESTNET_ACCOUNT_INFO_V2, data=data
        )

        if account_data is not None:
            for account in account_data["assets"]:
                balances[account["asset"]] = account

        return balances

    def place_order(
        self,
        symbol: str,
        side: Side,
        quantity: float,
        order_type: OrderType,
        price: Optional[float] = None,
        time_in_force: Optional[TimeInForce] = None,
    ):
        data = {}
        data["symbol"] = symbol
        data["side"] = side.value
        data["quantity"] = quantity
        data["type"] = order_type.value

        if price is not None:
            data["price"] = price

        if time_in_force is not None:
            data["timeInForce"] = time_in_force.value

        data["timestamp"] = int(time.time() * 1000)
        data["signature"] = self.generate_signature(data)

        order_status = self.make_request("POST", BINANCE_TESTNET_ORDER, data)
        return order_status

    def cancel_order(self, symbol: str, order_id: int):
        data = {}
        data["orderId"] = order_id
        data["symbol"] = symbol
        data["timestamp"] = int(time.time() * 1000)
        data["signature"] = self.generate_signature(data)

        order_status = self.make_request("DELETE", BINANCE_TESTNET_ORDER, data)
        return order_status

    def get_order_status(self, symbol: str, order_id: int):
        data = {}
        data["symbol"] = symbol
        data["orderId"] = order_id
        data["timestamp"] = int(time.time() * 1000)
        data["signature"] = self.generate_signature(data)

        order_status = self.make_request("GET", BINANCE_TESTNET_ORDER, data)

        return order_status
