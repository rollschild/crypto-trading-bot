import requests
from constants import BITMEX_API


def get_contracts():
    contracts = []

    response_object = requests.get(BITMEX_API)

    for contract in response_object.json():
        contracts.append(contract["symbol"])

    return contracts


print(get_contracts())
