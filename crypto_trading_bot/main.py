#!/usr/bin/env python

import tkinter as tk
import logging
import os
import pprint
from crypto_trading_bot.connectors.binance_futures import BinanceFuturesClient
from crypto_trading_bot.secrets import (
    BINANCE_TESTNET_API_KEY,
    BINANCE_TESTNET_API_SECRET,
)

from crypto_trading_bot.constants import OrderType, Side, TimeInForce


logger = logging.getLogger()
logger.setLevel(logging.INFO)

stream_handler = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s %(levelname)s :: %(message)s")
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.INFO)

current_directory = os.getcwd()
folder = "logs"
log_dir = os.path.join(current_directory, folder)
file_handler = logging.FileHandler(os.path.join(log_dir, "info.log"))
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)

logger.addHandler(stream_handler)
logger.addHandler(file_handler)

logger.debug("Debugging...")
logger.info("Basic info...")
logger.warning("FBI Warning")
logger.error("ERROR OCCURRED")

if __name__ == "__main__":
    binance = BinanceFuturesClient(
        True, BINANCE_TESTNET_API_KEY, BINANCE_TESTNET_API_SECRET
    )
    order_status = binance.place_order(
        "BTCUSDT", Side.BUY, 0.01, OrderType.LIMIT, 2000, TimeInForce.GTC
    )

    print("order status:")
    pprint.pprint(binance.get_order_status("BTCUSDT", order_status["orderId"]))

    # main window of the application
    root = tk.Tk()
    root.configure(bg="gray12")

    #  r = 0
    #  c = 0
    #  font = ("Calibri", 14, "normal")

    #  for contract in bitmex_contracts:
    #      label_widget = tk.Label(
    #          root,
    #          text=contract,
    #          bg="gray12",
    #          fg="SteelBlue1",
    #          borderwidth=1,
    #          relief=tk.SOLID,
    #          width=13,
    #          font=font,
    #      )
    #
    #      # label to fill the space east and west and be sticky
    #      label_widget.grid(row=r, column=c, sticky="ew")
    #
    #      if r == 4:
    #          c += 1
    #          r = 0
    #      else:
    #          r += 1
    #
    #  root.mainloop()
